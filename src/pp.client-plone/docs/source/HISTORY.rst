Changelog
=========

0.1.4 (12-07-2013)
-------------------

- major style and fonts cleanup

0.1.3 (11-07-2013)
-------------------

- various fixes
- Plone 4.0 - 4.2 compatibility

0.1.0 (11-07-2013)
-------------------

- initial release
